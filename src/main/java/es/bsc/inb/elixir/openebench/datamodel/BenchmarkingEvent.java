/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */
package es.bsc.inb.elixir.openebench.datamodel;

import java.net.URI;
import java.time.ZonedDateTime;
import java.util.List;
import javax.json.JsonObject;
import javax.json.bind.annotation.JsonbProperty;

/**
 * @author Dmitry Repchevsky
 */

public class BenchmarkingEvent {
    
    private String id;
    private String schema;
    private JsonObject metadata;
    private String original_id;
    private String name;
    private Boolean automated;
    private BenchmarkingEventDates dates;
    private URI url;    
    private String community_id;
    private List<String> contact_ids;
    private List<String> references;

    @JsonbProperty("_id")
    public String getID() {
        return id;
    }
    
    @JsonbProperty("_id")
    public void setID(String id) {
        this.id = id;
    }
    
    @JsonbProperty("_schema")
    public String getSchema() {
        return schema;
    }
    
    @JsonbProperty("_schema")
    public void setSchema(String schema) {
        this.schema = schema;
    }  
    
    @JsonbProperty("_metadata")
    public JsonObject getMetadata() {
        return metadata;
    }
    
    @JsonbProperty("_metadata")
    public void setMetadata(JsonObject metadata) {
        this.metadata = metadata;
    }
        
    @JsonbProperty("orig_id")
    public String getOriginalID() {
        return original_id;
    }
    
    @JsonbProperty("orig_id")
    public void setOriginalID(String original_id) {
        this.original_id = original_id;
    }

    @JsonbProperty("name")
    public String getName() {
        return name;
    }
    
    @JsonbProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonbProperty("is_automated")
    public Boolean getAutomated() {
        return automated;
    }
    
    @JsonbProperty("is_automated")
    public void setAutomated(Boolean automated) {
        this.automated = automated;
    }

    @JsonbProperty("dates")
    public BenchmarkingEventDates getDates() {
        return dates;
    }

    @JsonbProperty("dates")
    public void setDates(BenchmarkingEventDates dates) {
        this.dates = dates;
    }

    @JsonbProperty("url")
    public URI getURL() {
        return url;
    }

    @JsonbProperty("url")
    public void setURL(URI url) {
        this.url = url;
    }

    @JsonbProperty("community_id")
    public String getCommunityID() {
        return community_id;
    }
    
    @JsonbProperty("community_id")
    public void setCommunityID(String community_id) {
        this.community_id = community_id;
    }

    @JsonbProperty("bench_contact_ids")
    public List<String> getContactIDs() {
        return contact_ids;
    }
    
    @JsonbProperty("bench_contact_ids")
    public void setContactIDs(List<String> contact_ids) {
        this.contact_ids = contact_ids;
    }

    @JsonbProperty("references")
    public List<String> getReferences() {
        return references;
    }
    
    @JsonbProperty("references")
    public void setReferences(List<String> references) {
        this.references = references;
    }
    
    public static class BenchmarkingEventDates extends Dates {
        
        private ZonedDateTime benchmark_start;
        private ZonedDateTime benchmark_stop;

        @JsonbProperty("benchmark_start")
        public ZonedDateTime getBenchmarkStart() {
            return benchmark_start;
        }

        @JsonbProperty("benchmark_start")
        public void setBenchmarkStart(ZonedDateTime benchmark_start) {
            this.benchmark_start = benchmark_start;
        }

        @JsonbProperty("benchmark_stop")
        public ZonedDateTime getBenchmarkStop() {
            return benchmark_stop;
        }

        @JsonbProperty("benchmark_stop")
        public void setBenchmarkStop(ZonedDateTime benchmark_stop) {
            this.benchmark_stop = benchmark_stop;
        }
    }
}
