/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */
package es.bsc.inb.elixir.openebench.datamodel;

import java.net.URI;
import java.time.ZonedDateTime;
import java.util.List;
import javax.json.bind.annotation.JsonbProperty;

/**
 * @author Dmitry Repchevsky
 */

public class Challenge {
    
    private String id;
    private String schema;
    private String original_id;
    private String name;
    private String acronym;
    private String description;
    private String benchmarking_event_id;
    private Boolean automated;
    private ChallengeDates dates;
    private URI url;
    private MetricsCategories metrics_categories;    
    private List<String> challenge_contact_ids;
    private List<String> references;

    @JsonbProperty("_id")
    public String getID() {
        return id;
    }
    
    @JsonbProperty("_id")
    public void setID(String id) {
        this.id = id;
    }
    
    @JsonbProperty("_schema")
    public String getSchema() {
        return schema;
    }
    
    @JsonbProperty("_schema")
    public void setSchema(String schema) {
        this.schema = schema;
    }    

    @JsonbProperty("orig_id")
    public String getOriginalID() {
        return original_id;
    }
    
    @JsonbProperty("orig_id")
    public void setOriginalID(String original_id) {
        this.original_id = original_id;
    }

    @JsonbProperty("name")
    public String getName() {
        return name;
    }

    @JsonbProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonbProperty("acronym")
    public String getAcronym() {
        return acronym;
    }
    
    @JsonbProperty("acronym")
    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    @JsonbProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonbProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonbProperty("benchmarking_event_id")
    public String getBenchmarkingEventID() {
        return benchmarking_event_id;
    }

    @JsonbProperty("benchmarking_event_id")
    public void setBenchmarkingEventID(String benchmarking_event_id) {
        this.benchmarking_event_id = benchmarking_event_id;
    }

    @JsonbProperty("is_automated")
    public Boolean getAutomated() {
        return automated;
    }
    
    @JsonbProperty("is_automated")
    public void setAutomated(Boolean automated) {
        this.automated = automated;
    }

    @JsonbProperty("dates")
    public ChallengeDates getDates() {
        return dates;
    }

    @JsonbProperty("dates")
    public void setDates(ChallengeDates dates) {
        this.dates = dates;
    }

    @JsonbProperty("url")
    public URI getURL() {
        return url;
    }

    @JsonbProperty("url")
    public void setURL(URI url) {
        this.url = url;
    }

    @JsonbProperty("metrics_categories")
    public MetricsCategories getMetricsCategories() {
        return metrics_categories;
    }

    @JsonbProperty("metrics_categories")
    public void setMetricsCategories(MetricsCategories metrics_categories) {
        this.metrics_categories = metrics_categories;
    }

    @JsonbProperty("challenge_contact_ids")
    public List<String> getChallengeContactIDs() {
        return challenge_contact_ids;
    }

    @JsonbProperty("challenge_contact_ids")
    public void setChallengeContactIDs(List<String> challenge_contact_ids) {
        this.challenge_contact_ids = challenge_contact_ids;
    }

    @JsonbProperty("references")
    public List<String> getReferences() {
        return references;
    }

    @JsonbProperty("references")
    public void setReferences(List<String> references) {
        this.references = references;
    }

    public static class ChallengeDates extends Dates {
        
        private ZonedDateTime challenge_start;
        private ZonedDateTime challenge_stop;

        @JsonbProperty("challenge_start")
        public ZonedDateTime getChallengeStart() {
            return challenge_start;
        }

        @JsonbProperty("challenge_start")
        public void setChallengeStart(ZonedDateTime challenge_start) {
            this.challenge_start = challenge_start;
        }

        @JsonbProperty("challenge_stop")
        public ZonedDateTime getChallengeStop() {
            return challenge_stop;
        }

        @JsonbProperty("challenge_stop")
        public void setChallengeStop(ZonedDateTime challenge_stop) {
            this.challenge_stop = challenge_stop;
        }
    }

    public static class MetricsCategories {
        
        private String category;
        private String description;
        private ToolMetrics metrics;

        @JsonbProperty("category")
        public String getCategory() {
            return category;
        }

        @JsonbProperty("category")
        public void setCategory(String category) {
            this.category = category;
        }

        @JsonbProperty("description")
        public String getDescription() {
            return description;
        }

        @JsonbProperty("description")
        public void setDescription(String description) {
            this.description = description;
        }

        @JsonbProperty("metrics")
        public ToolMetrics getMetrics() {
            return metrics;
        }

        @JsonbProperty("metrics")
        public void setMetrics(ToolMetrics metrics) {
            this.metrics = metrics;
        }        
    }
    
    public static class ToolMetrics {
        
        private String metrics_id;
        private String tool_id;
        
        @JsonbProperty("metrics_id")
        public String getMetricsID() {
            return metrics_id;
        }

        @JsonbProperty("metrics_id")
        public void setMetricsID(String metrics_id) {
            this.metrics_id = metrics_id;
        }
    
        @JsonbProperty("tool_id")
        public String getToolID() {
            return tool_id;
        }

        @JsonbProperty("tool_id")
        public void setToolID(String tool_id) {
            this.tool_id = tool_id;
        }
    
    }
}
