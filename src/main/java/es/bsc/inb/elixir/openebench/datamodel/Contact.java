/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */
package es.bsc.inb.elixir.openebench.datamodel;

import java.util.List;
import javax.json.JsonObject;
import javax.json.bind.annotation.JsonbProperty;

/**
 * @author Dmitry Repchevsky
 */

public class Contact {
    
    private String id;
    private String schema;
    private JsonObject metadata;
    private String contact_type;
    private String given_name;
    private String surname;
    private List<String> emails;
    private List<String> community_ids;
    private String notes;
    private List<Link> links;
    
    @JsonbProperty("_id")
    public String getID() {
        return id;
    }
    
    @JsonbProperty("_id")
    public void setID(String id) {
        this.id = id;
    }
    
    @JsonbProperty("_schema")
    public String getSchema() {
        return schema;
    }
    
    @JsonbProperty("_schema")
    public void setSchema(String schema) {
        this.schema = schema;
    }
    
    @JsonbProperty("_metadata")
    public JsonObject getMetadata() {
        return metadata;
    }
    
    @JsonbProperty("_metadata")
    public void setMetadata(JsonObject metadata) {
        this.metadata = metadata;
    }
    
    @JsonbProperty("contact_type")
    public String getContactType() {
        return contact_type;
    }
    
    @JsonbProperty("contact_type")
    public void setContactType(String contact_type) {
        this.contact_type = contact_type;
    }
    
    @JsonbProperty("givenName")
    public String getGivenName() {
        return given_name;
    }
    
    @JsonbProperty("givenName")
    public void setGivenName(String given_name) {
        this.given_name = given_name;
    }

    @JsonbProperty("surname")
    public String getSurname() {
        return surname;
    }
    
    @JsonbProperty("surname")
    public void setSurname(String surname) {
        this.surname = surname;
    }
    
    @JsonbProperty("emails")
    public List<String> getEmails() {
        return emails;
    }
    
    @JsonbProperty("emails")
    public void setEmails(List<String> emails) {
        this.emails = emails;
    }
    
    @JsonbProperty("community_ids")
    public List<String> getCommunityIDs() {
        return community_ids;
    }
    
    @JsonbProperty("community_ids")
    public void setCommunityIDs(List<String> community_ids) {
        this.community_ids = community_ids;
    }

    @JsonbProperty("notes")
    public String getNotes() {
        return notes;
    }
    
    @JsonbProperty("notes")
    public void setNotes(String notes) {
        this.notes = notes;
    }
    
    @JsonbProperty("links")
    public List<Link> getSocialContacts() {
        return links;
    }
    
    @JsonbProperty("links")
    public void setSocialContacts(List<Link> links) {
        this.links = links;
    }
}
