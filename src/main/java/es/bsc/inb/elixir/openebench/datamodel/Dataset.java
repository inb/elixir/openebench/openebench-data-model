/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */
package es.bsc.inb.elixir.openebench.datamodel;

import java.util.List;
import javax.json.JsonObject;
import javax.json.bind.annotation.JsonbProperty;

/**
 * @author Dmitry Repchevsky
 */

public class Dataset {
    private String id;
    private String schema;
    private JsonObject metadata;
    private String original_id;
    private List<String> community_ids;
    private List<String> challenge_ids;
    private String visibility;
    private String name;
    private String version;
    private String description;
    private Dates dates;
    private String type;

    private Datalink datalink;
    private List<String> dataset_contact_ids;
    private DatasetTool depends_on;
    private List<String> references;

    @JsonbProperty("_id")
    public String getID() {
        return id;
    }
    
    @JsonbProperty("_id")
    public void setID(String id) {
        this.id = id;
    }
    
    @JsonbProperty("_schema")
    public String getSchema() {
        return schema;
    }
    
    @JsonbProperty("_schema")
    public void setSchema(String schema) {
        this.schema = schema;
    }    

    @JsonbProperty("_metadata")
    public JsonObject getMetadata() {
        return metadata;
    }
    
    @JsonbProperty("_metadata")
    public void setMetadata(JsonObject metadata) {
        this.metadata = metadata;
    }

    @JsonbProperty("orig_id")
    public String getOriginalID() {
        return original_id;
    }
    
    @JsonbProperty("orig_id")
    public void setOriginalID(String original_id) {
        this.original_id = original_id;
    }

    @JsonbProperty("community_ids")
    public List<String> getCommunityIDs() {
        return community_ids;
    }

    @JsonbProperty("community_ids")
    public void setCommunityIDs(List<String> community_ids) {
        this.community_ids = community_ids;
    }

    @JsonbProperty("challenge_ids")
    public List<String> getChallengeIDs() {
        return challenge_ids;
    }

    @JsonbProperty("challenge_ids")
    public void setChallengeIDs(List<String> challenge_ids) {
        this.challenge_ids = challenge_ids;
    }

    @JsonbProperty("visibility")
    public String getVisibility() {
        return visibility;
    }

    @JsonbProperty("visibility")
    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    @JsonbProperty("name")
    public String getName() {
        return name;
    }

    @JsonbProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonbProperty("version")
    public String getVersion() {
        return version;
    }

    @JsonbProperty("version")
    public void setVersion(String version) {
        this.version = version;
    }

    @JsonbProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonbProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonbProperty("dates")
    public Dates getDates() {
        return dates;
    }

    @JsonbProperty("dates")
    public void setDates(Dates dates) {
        this.dates = dates;
    }

    @JsonbProperty("type")
    public String getType() {
        return type;
    }

    @JsonbProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonbProperty("datalink")
    public Datalink getDatalink() {
        return datalink;
    }

    @JsonbProperty("datalink")
    public void setDatalink(Datalink datalink) {
        this.datalink = datalink;
    }

    @JsonbProperty("dataset_contact_ids")
    public List<String> getDatasetContactIDs() {
        return dataset_contact_ids;
    }

    @JsonbProperty("dataset_contact_ids")
    public void setDatasetContactIDs(List<String> dataset_contact_ids) {
        this.dataset_contact_ids = dataset_contact_ids;
    }

    @JsonbProperty("depends_on")
    public DatasetTool getDependsOn() {
        return depends_on;
    }

    @JsonbProperty("depends_on")
    public void setDependsOn(DatasetTool depends_on) {
        this.depends_on = depends_on;
    }

    @JsonbProperty("references")
    public List<String> getReferences() {
        return references;
    }

    @JsonbProperty("references")
    public void setReferences(List<String> references) {
        this.references = references;
    }
    
    public static class DatasetTool {
        
        private String metrics_id;
        private String tool_id;
        private List<OriginatedDataset> related_dataset_ids;
        
        @JsonbProperty("metrics_id")
        public String getMetricsID() {
            return metrics_id;
        }

        @JsonbProperty("metrics_id")
        public void setMetricsID(String metrics_id) {
            this.metrics_id = metrics_id;
        }

        @JsonbProperty("tool_id")
        public String getToolID() {
            return tool_id;
        }

        @JsonbProperty("tool_id")
        public void setToolID(String tool_id) {
            this.tool_id = tool_id;
        }

        @JsonbProperty("rel_dataset_ids")
        public List<OriginatedDataset> getRelatedDatasetIDs() {
            return related_dataset_ids;
        }

        @JsonbProperty("rel_dataset_ids")
        public void setRelatedDatasetIDs(List<OriginatedDataset> related_dataset_ids) {
            this.related_dataset_ids = related_dataset_ids;
        }
    }
    
    public static class OriginatedDataset {

        private String dataset_id;
        private String role;
        
        @JsonbProperty("dataset_id")
        public String getDatasetID() {
            return dataset_id;
        }

        @JsonbProperty("dataset_id")
        public void setDatasetID(String dataset_id) {
            this.dataset_id = dataset_id;
        }

        @JsonbProperty("role")
        public String getRole() {
            return role;
        }

        @JsonbProperty("role")
        public void setRole(String role) {
            this.role = role;
        }
    }
}
