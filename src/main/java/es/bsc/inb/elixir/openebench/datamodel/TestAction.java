/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */
package es.bsc.inb.elixir.openebench.datamodel;

import java.time.ZonedDateTime;
import java.util.List;
import javax.json.JsonObject;
import javax.json.bind.annotation.JsonbProperty;

/**
 * @author Dmitry Repchevsky
 */

public class TestAction {
    private String id;
    private String schema;
    private JsonObject metadata;
    private String original_id;
    private String tool_id;
    private String action_type;
    private List<ActionDataset> involved_datasets; 
    private String challenge_id;
    private List<String> test_contact_ids;
    private String status;
    private Dates dates;

    @JsonbProperty("_id")
    public String getID() {
        return id;
    }
    
    @JsonbProperty("_id")
    public void setID(String id) {
        this.id = id;
    }
    
    @JsonbProperty("_schema")
    public String getSchema() {
        return schema;
    }
    
    @JsonbProperty("_schema")
    public void setSchema(String schema) {
        this.schema = schema;
    }  
    
    @JsonbProperty("_metadata")
    public JsonObject getMetadata() {
        return metadata;
    }
    
    @JsonbProperty("_metadata")
    public void setMetadata(JsonObject metadata) {
        this.metadata = metadata;
    }
        
    @JsonbProperty("orig_id")
    public String getOriginalID() {
        return original_id;
    }
    
    @JsonbProperty("orig_id")
    public void setOriginalID(String original_id) {
        this.original_id = original_id;
    }
    
    @JsonbProperty("tool_id")
    public String getToolID() {
        return tool_id;
    }
    
    @JsonbProperty("tool_id")
    public void setToolID(String tool_id) {
        this.tool_id = tool_id;
    }

    @JsonbProperty("action_type")
    public String getActionType() {
        return action_type;
    }
    
    @JsonbProperty("action_type")
    public void setActionType(String action_type) {
        this.action_type = action_type;
    }
    
    @JsonbProperty("involved_datasets")
    public List<ActionDataset> getInvolvedDatasets() {
        return involved_datasets;
    }
    
    @JsonbProperty("involved_datasets")
    public void setInvolvedDatasets(List<ActionDataset> involved_datasets) {
        this.involved_datasets = involved_datasets;
    }

    @JsonbProperty("challenge_id")
    public String getChallengeID() {
        return challenge_id;
    }
    
    @JsonbProperty("challenge_id")
    public void setChallengeID(String challenge_id) {
        this.challenge_id = challenge_id;
    }

    @JsonbProperty("test_contact_ids")
    public List<String> getTestContactIDs() {
        return test_contact_ids;
    }
    
    @JsonbProperty("test_contact_ids")
    public void setTestContactIDs(List<String> test_contact_ids) {
        this.test_contact_ids = test_contact_ids;
    }
    
    @JsonbProperty("status")
    public String getStatus() {
        return status;
    }
    
    @JsonbProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonbProperty("dates")
    public Dates getDates() {
        return dates;
    }

    @JsonbProperty("dates")
    public void setDates(Dates dates) {
        this.dates = dates;
    }

    public static class ActionDataset {
        
        private String dataset_id;
        private String role;
        private ZonedDateTime reception_date;
        
        @JsonbProperty("dataset_id")
        public String getDatasetID() {
            return dataset_id;
        }

        @JsonbProperty("dataset_id")
        public void setDatasetID(String dataset_id) {
            this.dataset_id = dataset_id;
        }

        @JsonbProperty("role")
        public String getRole() {
            return role;
        }

        @JsonbProperty("role")
        public void setRole(String role) {
            this.role = role;
        }
        
        @JsonbProperty("received")
        public ZonedDateTime getReceptionDate() {
            return reception_date;
        }

        @JsonbProperty("received")
        public void setReceptionDate(ZonedDateTime reception_date) {
            this.reception_date = reception_date;
        }
    }
}
