/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */
package es.bsc.inb.elixir.openebench.datamodel.adapter;

import es.bsc.inb.elixir.openebench.datamodel.Datalink;
import es.bsc.inb.elixir.openebench.datamodel.Datalink.ExternalDataset;
import es.bsc.inb.elixir.openebench.datamodel.Datalink.InlinedData;
import java.lang.reflect.Type;
import javax.json.JsonObject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.serializer.DeserializationContext;
import javax.json.bind.serializer.JsonbDeserializer;
import javax.json.stream.JsonParser;

/**
 * @author Dmitry Repchevsky
 */

public class DatalinkDeserializer implements JsonbDeserializer<Datalink> {

    private final Jsonb jsonb = JsonbBuilder.create();
    
    @Override
    public Datalink deserialize(JsonParser parser, DeserializationContext ctx, Type type) {
        final JsonObject obj = parser.getObject();
        
        final String inline_data = obj.getString("inline_data", null);
        if (inline_data != null) {
            return jsonb.fromJson(obj.toString(), InlinedData.class);
        }
        
        final String uri = obj.getString("uri", null);
        if (uri != null) {
            return jsonb.fromJson(obj.toString(), ExternalDataset.class);
        }

        return null;
    }
    
}
