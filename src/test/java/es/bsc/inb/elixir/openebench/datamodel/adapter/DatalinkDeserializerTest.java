/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */
package es.bsc.inb.elixir.openebench.datamodel.adapter;

import es.bsc.inb.elixir.openebench.datamodel.Datalink;
import es.bsc.inb.elixir.openebench.datamodel.Datalink.ExternalDataset;
import es.bsc.inb.elixir.openebench.datamodel.Dataset;
import java.net.URI;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Dmitry Repchevsky
 */

public class DatalinkDeserializerTest {
   
    final String DATSET_JSON = "{ \"datalink\" : {\"uri\" : \"http://bsc.es\"} }";
    
    @Test
    public void deserialize_interface() {
        
        final Jsonb jsonb = JsonbBuilder.create();
        
        final Dataset dataset = jsonb.fromJson(DATSET_JSON, Dataset.class);
        Assert.assertNotNull(dataset);
        
        final Datalink datalink = dataset.getDatalink();
        Assert.assertTrue(datalink instanceof ExternalDataset);
        
        Assert.assertEquals(URI.create("http://bsc.es"), ((ExternalDataset)datalink).getURI());
    }
}
